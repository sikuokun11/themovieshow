var block_actors = 'block-actors';
var caroufredsel_wrapper = 'caroufredsel_wrapper';
var list_actor_carousel = 'list_actor_carousel';
var pagination = 'pagination';
var current_page = 'current-page';
var previous_page = 'previous-page';
var next_page = 'next-page';
var thanhchiatrang = 'thanhchiatrang';
var each_movie = 'each-movie';
var loader_wrapper = 'loader-wrapper';
$(document).ready(function() {
    var apiBaseURL = 'http://api.themoviedb.org/3/';


    var imageBaseUrl = 'https://image.tmdb.org/t/p/';
    var apiKey = '53ddf3f904e38ff4e8418794a5da4e88';

    const nowPlayingURL = apiBaseURL + 'movie/top_rated?api_key=' + apiKey;

    function getNowPlayingData() {
        $.getJSON(nowPlayingURL, function(nowPlayingData) {

            for (let i = 0; i < nowPlayingData.results.length; i++) {
                // w300 
                var mid = nowPlayingData.results[i].id;
                // mid = movie ID
                var thisMovieUrl = apiBaseURL + 'movie/' + mid + '/videos?api_key=' + apiKey;
                // console.log(i)
                var detailUrl = apiBaseURL + 'movie/' + mid + '?api_key=' + apiKey;


                var category = '';
                $.getJSON(detailUrl, function(movieKey) {
                    //console.log(movieKey);
                    category = '';
                    for (let j = 0; j < movieKey.genres.length; j++) {
                        if (category != '') {
                            category = category + ', ' + movieKey.genres[j].name;
                        } else {
                            category = category + movieKey.genres[j].name;
                        }
                    }
                    return category;
                })

                $.getJSON(thisMovieUrl, function(movieKey) {
                    // console.log(i);
                    // console.log(thisMovieUrl)
                    // console.log(movieKey)


                    const mid2 = nowPlayingData.results[i].id;
                    var poster = imageBaseUrl + 'w300' + nowPlayingData.results[i].poster_path;
                    // console.log(poster);

                    var title = nowPlayingData.results[i].original_title;

                    var releaseDate = nowPlayingData.results[i].release_date;

                    var overview = nowPlayingData.results[i].overview;
                    // $('.overview').addClass('overview');

                    var voteAverage = nowPlayingData.results[i].vote_average;
                    // console.log(movieKey)
                    var youtubeKey = movieKey.results[0].key;

                    var youtubeLink = 'https://www.youtube.com/watch?v=' + youtubeKey;
                    // console.log(youtubeLink)

                    var nowPlayingHTML = '';

                    nowPlayingHTML += '<div class="col-sm-3 eachMovie">';
                    nowPlayingHTML += '<button type="button" class="btnModal" data-toggle="modal" data-target="#exampleModal' + i + '" data-whatever="@' + i + '">' + '<img src="' + poster + '"></button>';
                    nowPlayingHTML += '<div class="modal fade" id="exampleModal' + i + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
                    nowPlayingHTML += '<div class="modal-dialog" role="document">';
                    nowPlayingHTML += '<div class="modal-content col-sm-12">';
                    nowPlayingHTML += '<div class="col-sm-6 moviePosterInModal">';
                    nowPlayingHTML += '<a href="' + youtubeLink + '"><img src="' + poster + '"></a>';
                    nowPlayingHTML += '</div><br>';
                    nowPlayingHTML += '<div class="col-sm-6 movieDetails">';
                    nowPlayingHTML += '<div class="movieName">' + title + '</div><br>';
                    nowPlayingHTML += '<div class="linkToTrailer"><a href="' + youtubeLink + '"><span class="glyphicon glyphicon-play"></span>&nbspPlay trailer</a>' + '</div><br>';
                    nowPlayingHTML += '<div class="release">Release Date: ' + releaseDate + '</div><br>';
                    nowPlayingHTML += '<div class="genre">Genre: ' + category + '</div><br>';
                    nowPlayingHTML += '<div class="overview">' + overview + '</div><br>';
                    nowPlayingHTML += '<div class="rating">Rating: ' + voteAverage + '/10</div><br>';
                    // nowPlayingHTML += ' <div class="navbar-header" style=" background: rgba(0, 0, 0, 0.6);" ><a class="navbar-brand" href="#" id="magicbutton">MoreInfo</a></div>';
                    nowPlayingHTML += '<button class="btn btn-default" onclick="movieDetail(' + mid2 + ');">MoreInfo</button>';
                    nowPlayingHTML += '</div>';
                    nowPlayingHTML += '</div>';
                    nowPlayingHTML += '</div>';
                    nowPlayingHTML += '</div>';
                    nowPlayingHTML += '</div>';

                    $('#movie-grid').append(nowPlayingHTML);

                    $('#movieGenreLabel').html("Top Rated");



                })
            }
        })
    }


    // Check: 
    // http://api.themoviedb.org/3/movie/:movieID?api_key=<<>>


    function getMoviesByGenre(genre_id) {
        const getMoviesByGenreURL = apiBaseURL + 'genre/' + genre_id + '/movies?api_key=' + apiKey + '&language=en-US&include_adult=false&sort_by=created_at.asc';


        $.getJSON(getMoviesByGenreURL, function(genreData) {
            // console.log(genreData)
            for (let i = 0; i < genreData.results.length; i++) {
                var mid = genreData.results[i].id;
                var thisMovieUrl = apiBaseURL + 'movie/' + mid + '/videos?api_key=' + apiKey;

                $.getJSON(thisMovieUrl, function(movieKey) {
                    var poster = imageBaseUrl + 'w300' + genreData.results[i].poster_path;
                    var title = genreData.results[i].original_title;
                    var releaseDate = genreData.results[i].release_date;
                    var overview = genreData.results[i].overview;
                    var voteAverage = genreData.results[i].vote_average;
                    var youtubeKey = movieKey.results[0].key;
                    var youtubeLink = 'https://www.youtube.com/watch?v=' + youtubeKey;
                    var genreHTML = '';
                    genreHTML += '<div class="col-sm-3 col-md-3 col-lg-3 eachMovie">';
                    genreHTML += '<button type="button" class="btnModal" data-toggle="modal" data-target="#exampleModal' + i + '" data-whatever="@' + i + '">' + '<img src="' + poster + '"></button>';
                    genreHTML += '<div class="modal fade" id="exampleModal' + i + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
                    genreHTML += '<div class="modal-dialog" role="document">';
                    genreHTML += '<div class="modal-content col-sm-12 col-lg-12">';
                    genreHTML += '<div class="col-sm-6 moviePosterInModal">';
                    genreHTML += '<a href="' + youtubeLink + '"><img src="' + poster + '"></a>';
                    genreHTML += '</div><br>';
                    genreHTML += '<div class="col-sm-6 movieDetails">';
                    genreHTML += '<div class="movieName">' + title + '</div><br>';
                    genreHTML += '<div class="linkToTrailer"><a href="' + youtubeLink + '"><span class="glyphicon glyphicon-play"></span>&nbspPlay trailer</a>' + '</div><br>';
                    genreHTML += '<div class="release">Release Date: ' + releaseDate + '</div><br>';
                    genreHTML += '<div class="overview">' + overview + '</div><br>';
                    genreHTML += '<div class="rating">Rating: ' + voteAverage + '/10</div><br>';
                    genreHTML += '<div class="col-sm-3 btn btn-primary">8:30 AM' + '</div>';
                    genreHTML += '<div class="col-sm-3 btn btn-primary">10:00 AM' + '</div>';
                    genreHTML += '<div class="col-sm-3 btn btn-primary">12:30 PM' + '</div>';
                    genreHTML += '<div class="col-sm-3 btn btn-primary">3:00 PM' + '</div>';
                    genreHTML += '<div class="col-sm-3 btn btn-primary">4:10 PM' + '</div>';
                    genreHTML += '<div class="col-sm-3 btn btn-primary">5:30 PM' + '</div>';
                    genreHTML += '<div class="col-sm-3 btn btn-primary">8:00 PM' + '</div>';
                    genreHTML += '<div class="col-sm-3 btn btn-primary">10:30 PM' + '</div>';
                    genreHTML += '</div>'; //
                    genreHTML += '</div>'; //
                    genreHTML += '</div>';
                    genreHTML += '</div>';
                    genreHTML += '</div>';
                    $('#movie-grid').append(genreHTML);

                })
            }
        })
    }

    getNowPlayingData();


    var nowPlayingHTML = '';
    var genreHTML = '';

    $('.navbar-brand').click(function() {
        getNowPlayingData();
        $('#movie-grid').html(nowPlayingHTML);
        $('#movieGenreLabel').html("WelCome");
    })
    $('.topRate').click(function() {
        getNowPlayingData();
        $('#movie-grid').html(nowPlayingHTML);
        $('#movieGenreLabel').html("Top Rated");
    })
    $('#action').click(function() {
        getMoviesByGenre(28);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Action");
    })
    $('#adventure').click(function() {
        getMoviesByGenre(12);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Adventure");
    })
    $('#animation').click(function() {
        getMoviesByGenre(16);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Animation");
    })
    $('#comedy').click(function() {
        getMoviesByGenre(35);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Comedy");
    })
    $('#crime').click(function() {
        getMoviesByGenre(80);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Crime");
    })
    $('#drama').click(function() {
        getMoviesByGenre(18);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Drama");
    })
    $('#family').click(function() {
        getMoviesByGenre(10751);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Family");
    })
    $('#fantasy').click(function() {
        getMoviesByGenre(14);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Fantasy");
    })
    $('#history').click(function() {
        getMoviesByGenre(36);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("History");
    })
    $('#horror').click(function() {
        getMoviesByGenre(27);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Horror");
    })
    $('#music').click(function() {
        getMoviesByGenre(10402);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Music");
    })
    $('#romance').click(function() {
        getMoviesByGenre(10749);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Romance");
    })
    $('#scifi').click(function() {
        getMoviesByGenre(878);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Science Fiction");
    })
    $('#thriller').click(function() {
        getMoviesByGenre(53);
        $('#movie-grid').html(genreHTML);
        $('#movieGenreLabel').html("Thriller");
    })

    //==============================================================================
    //====================== Search Function =======================================
    //==============================================================================



    var searchTerm = '';
    var searchTerm2 = '';
    //searchMovies();
    //searchMoviesByPerson();

    function removeXXX() {
        $("#" + loader_wrapper).fadeOut();
    }


    $('.searchForm').submit(function(event) {
        $('#movie-grid').empty();
        let loadingsite = `<div id="loader-wrapper">
        <div id="loader1" class="loader"></div><div id="loader2" class="loader"></div>
        <div id="loader3" class="loader"></div>
        <div id="loader4" class="loader"></div>
        <div id="loader5" class="loader"></div>
      </div>`;
        //$('#movie-grid').append(loadingsite);
        $('#movie-grid').html(loadingsite);
        setTimeout(removeXXX(), 10000);
        event.preventDefault();
        searchTerm = $('.form-control').val();
        searchMovies();
    })
    $('.searchForm2').submit(function(event) {
        $('#movie-grid').empty();

        let loadingsite = `<div id="loader-wrapper">
        <div id="loader1" class="loader"></div><div id="loader2" class="loader"></div>
        <div id="loader3" class="loader"></div>
        <div id="loader4" class="loader"></div>
        <div id="loader5" class="loader"></div>
      </div>`;
        //$('#movie-grid').append(loadingsite);
        $('#movie-grid').html(loadingsite);
        setTimeout(removeXXX(), 10000);
        event.preventDefault();
        searchTerm2 = $('.form-control2').val();
        searchMoviesByPerson();
    })

    function searchMovies() {
        //need to include query in url. (ex: &query=boss+baby)
        const searchMovieURL = apiBaseURL + 'search/movie?api_key=' + apiKey + '&language=en-US&page=1&include_adult=false&query=' + searchTerm;
        // console.log(searchMovieURL);
        $.getJSON(searchMovieURL, function(movieSearchResults) {
            // console.log(movieSearchResults);
            for (let i = 0; i < movieSearchResults.results.length; i++) {
                var mid = movieSearchResults.results[i].id;
                var thisMovieUrl = apiBaseURL + 'movie/' + mid + '/videos?api_key=' + apiKey;

                $.getJSON(thisMovieUrl, function(movieKey) {
                    // console.log(movieKey)
                    var poster = imageBaseUrl + 'w300' + movieSearchResults.results[i].poster_path;
                    var title = movieSearchResults.results[i].original_title;
                    var releaseDate = movieSearchResults.results[i].release_date;
                    var overview = movieSearchResults.results[i].overview;
                    const mid2 = movieSearchResults.results[i].id;
                    var voteAverage = movieSearchResults.results[i].vote_average;
                    var youtubeKey = movieKey.results[0].key;
                    var youtubeLink = 'https://www.youtube.com/watch?v=' + youtubeKey;
                    var searchResultsHTML = '';
                    searchResultsHTML += '<div class="col-sm-3 col-md-3 col-lg-3 eachMovie">';
                    searchResultsHTML += '<button type="button" class="btnModal" data-toggle="modal" data-target="#exampleModal' + i + '" data-whatever="@' + i + '">' + '<img src="' + poster + '"></button>';
                    searchResultsHTML += '<div class="modal fade" id="exampleModal' + i + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
                    searchResultsHTML += '<div class="modal-dialog" role="document">';
                    searchResultsHTML += '<div class="modal-content col-sm-12 col-lg-12">';
                    searchResultsHTML += '<div class="col-sm-6 moviePosterInModal">';
                    searchResultsHTML += '<a href="' + youtubeLink + '"><img src="' + poster + '"></a>';
                    searchResultsHTML += '</div><br>'; //close trailerLink
                    searchResultsHTML += '<div class="col-sm-6 movieDetails">';
                    searchResultsHTML += '<div class="movieName">' + title + '</div><br>';
                    searchResultsHTML += '<div class="linkToTrailer"><a href="' + youtubeLink + '"><span class="glyphicon glyphicon-play"></span>&nbspPlay trailer</a>' + '</div><br>';
                    searchResultsHTML += '<div class="release">Release Date: ' + releaseDate + '</div><br>';
                    searchResultsHTML += '<div class="overview">' + overview + '</div><br>';
                    searchResultsHTML += '<div class="rating">Rating: ' + voteAverage + '/10</div><br>';
                    searchResultsHTML += '<button class="btn btn-default" onclick="movieDetail2(' + mid2 + ');">MoreInfo</button>';

                    // searchResultsHTML += '<button class="btn btn-default" onclick="movieDetail2(' + searchTerm + '/' + mid2 + ');">MoreInfo</button>';
                    searchResultsHTML += '</div>'; //close movieDetails
                    searchResultsHTML += '</div>'; //close modal-dialog
                    searchResultsHTML += '</div>'; //close modal
                    searchResultsHTML += '</div>'; //close off each div
                    // console.log(searchResultsHTML)
                    $('#movie-grid').append(searchResultsHTML);
                    //Label will be whatever user input was
                    $('#movieGenreLabel').html(searchTerm);
                })
            }
        })
    }

    function searchMoviesByPerson() {
        //need to include query in url. (ex: &query=boss+baby)
        const searchMovieURL = apiBaseURL + 'search/person?api_key=' + apiKey + '&language=en-US&page=1&include_adult=false&query=' + searchTerm2;
        //console.log(searchMovieURL);
        $.getJSON(searchMovieURL, function(movieSearchResults) {
            // console.log(movieSearchResults);
            for (let i = 0; i < movieSearchResults.results.length; i++) {
                var mid = movieSearchResults.results[i].id;
                //console.log(mid);
                var thisPersonMoviesURL = apiBaseURL + 'person/' + mid + '/movie_credits?api_key=' + apiKey;
                // var thisMovieUrl = apiBaseURL + 'movie/' + mid + '/videos?api_key=' + apiKey;

                $.getJSON(thisPersonMoviesURL, function(movieKey) {
                    // console.log(movieKey)
                    for (let i = 0; i < movieKey.cast.length; i++) {
                        var poster = imageBaseUrl + 'w300' + movieKey.cast[i].poster_path;
                        var title = movieKey.cast[i].original_title;
                        var releaseDate = movieKey.cast[i].release_date;
                        var overview = movieKey.cast[i].overview;
                        const mid2 = movieKey.cast[i].id;
                        var voteAverage = movieKey.cast[i].vote_average;
                        // var youtubeKey = movieKey.results[0].key;
                        var youtubeLink = 'https://www.youtube.com/watch?v=0';
                        var searchResultsHTML = '';
                        searchResultsHTML += '<div class="col-sm-3 col-md-3 col-lg-3 eachMovie">';
                        searchResultsHTML += '<button type="button" class="btnModal" data-toggle="modal" data-target="#exampleModal' + i + '" data-whatever="@' + i + '">' + '<img src="' + poster + '"></button>';
                        searchResultsHTML += '<div class="modal fade" id="exampleModal' + i + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
                        searchResultsHTML += '<div class="modal-dialog" role="document">';
                        searchResultsHTML += '<div class="modal-content col-sm-12 col-lg-12">';
                        searchResultsHTML += '<div class="col-sm-6 moviePosterInModal">';
                        searchResultsHTML += '<a href="' + youtubeLink + '"><img src="' + poster + '"></a>';
                        searchResultsHTML += '</div><br>'; //close trailerLink
                        searchResultsHTML += '<div class="col-sm-6 movieDetails">';
                        searchResultsHTML += '<div class="movieName">' + title + '</div><br>';
                        searchResultsHTML += '<div class="linkToTrailer"><a href="' + youtubeLink + '"><span class="glyphicon glyphicon-play"></span>&nbspPlay trailer</a>' + '</div><br>';
                        searchResultsHTML += '<div class="release">Release Date: ' + releaseDate + '</div><br>';
                        searchResultsHTML += '<div class="overview">' + overview + '</div><br>';
                        searchResultsHTML += '<div class="rating">Rating: ' + voteAverage + '/10</div><br>';
                        searchResultsHTML += '<div class="col-sm-3 btn btn-primary">8:30 AM' + '</div>';
                        searchResultsHTML += '<button class="btn btn-default" onclick="movieDetail3(' + mid2 + ');">MoreInfo</button>';
                        searchResultsHTML += '</div>'; //close movieDetails
                        searchResultsHTML += '</div>'; //close modal-dialog
                        searchResultsHTML += '</div>'; //close modal
                        searchResultsHTML += '</div>'; //close off each div
                        // console.log(searchResultsHTML)
                        $('#movie-grid').append(searchResultsHTML);
                        //Label will be whatever user input was
                        $('#movieGenreLabel').html(searchTerm2);
                    }
                })
            }
        })
    }
});

function movieDetail(Mid) {
    $('#movie-grid').empty();
    var mid = Mid;
    //var mid = 769;
    $("body").removeClass("modal-open");
    $("div").removeClass("modal-backdrop fade in");
    var apiBaseURL = 'http://api.themoviedb.org/3/';

    var imageBaseUrl = 'https://image.tmdb.org/t/p/';

    var apiKey = '53ddf3f904e38ff4e8418794a5da4e88';
    var thisMovieUrl = apiBaseURL + 'movie/' + mid + '/videos?api_key=' + apiKey;
    // console.log(thisMovieUrl);
    var thisMovieCharacterUrl = apiBaseURL + 'movie/' + mid + '/credits?api_key=' + apiKey;
    // console.log(thisMovieCharacterUrl);
    var thisMovieReviews = apiBaseURL + 'movie/' + mid + '/reviews?api_key=' + apiKey;
    var detailUrl = apiBaseURL + 'movie/' + mid + '?api_key=' + apiKey;
    var nowPlayingURL = apiBaseURL + 'movie/top_rated?api_key=' + apiKey;
    $.getJSON(nowPlayingURL, function(nowPlayingData) {
        var actorInforHTML = '';
        var directorInforHTML = '';
        for (let i = 0; i < nowPlayingData.results.length; i++) {
            if (nowPlayingData.results[i].id === mid) {
                $.getJSON(thisMovieCharacterUrl, function(movieKey) {

                    actorInforHTML = '';
                    // console.log(movieKey);
                    for (let k = 0; k < movieKey.cast.length; k++) {
                        var realName = movieKey.cast[k].name;
                        //console.log("ACTOR: " + realName);
                        var nameCharacter = movieKey.cast[k].character;
                        //console.log("IN DISPLAY: " + nameCharacter);
                        var imgCharacter = imageBaseUrl + 'w300' + movieKey.cast[k].profile_path;
                        //console.log(imgCharacter);
                        const actorId = movieKey.cast[k].id;

                        actorInforHTML += '<div class="block-actors">';
                        actorInforHTML += ' <div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 535px; height: 170px; margin: 0px; overflow: hidden;">';
                        actorInforHTML += '<ul class="row" id="list_actor_carousel" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 3531px; height: 170px; z-index: auto;"> <li style="display: inline;">';
                        actorInforHTML += ' <a class="actor-profile-item" href="#" onclick ="actorDetail(' + actorId + ');"  title="' + realName + ' Trong Vai ' + nameCharacter + '">';
                        actorInforHTML += '<div class="actor-image"><img src="' + imgCharacter + '"></div>';
                        actorInforHTML += '<div class="actor-name"><span class="actor-name-a">' + realName + '</span><span class="character">' + ' ' + nameCharacter + '</span></div>';
                        actorInforHTML += ' </a>';
                        actorInforHTML += ' </li>';
                        actorInforHTML += ' </ul></div></div>';
                    }
                    //actorInforHTML += ' <nav aria-label=...><ul class=pagination><li id="previous-page"><a href="javascript:void(0)" aria-label=Previous><span aria-hidden=true>&laquo;</span></a></li></ul><nav>';
                    //makePagination(movieKey.cast.length);
                    directorInforHTML = '';
                    for (let l = 0; l < movieKey.crew.length; l++) {
                        var jobForce = movieKey.crew[l].job;
                        if (jobForce.localeCompare("Director") == 0) {
                            var directorName = movieKey.crew[l].name;
                            var directorImage = imageBaseUrl + 'w300' + movieKey.crew[l].profile_path;

                            directorInforHTML += '<div class="block-director">';
                            directorInforHTML += ' <a class="director-profile-item" href="#" title="' + directorName + '">';
                            directorInforHTML += '<div class="director-image"><img src="' + directorImage + '"></div>';
                            directorInforHTML += ' <div class="director-name" ><b>' + directorName + '</b></div>';
                            directorInforHTML += ' </a>';
                            directorInforHTML += '</div>';
                            //console.log(directorImage);
                        }
                    }
                })

                var category;

                $.getJSON(detailUrl, function(movieKey) {
                    //console.log(movieKey);
                    category = '';
                    for (let j = 0; j < movieKey.genres.length; j++) {
                        if (category != '') {
                            category = category + ', ' + movieKey.genres[j].name;
                        } else {
                            category = category + movieKey.genres[j].name;
                        }
                    }
                    return category;
                })
                var reviewsHTML = '';
                $.getJSON(thisMovieReviews, function(movieKey) {
                    reviewsHTML = '';
                    for (let m = 0; m < 2; m++) {
                        var Reviewer = movieKey.results[m].author;
                        //console.log(Reviewer);
                        var Content = movieKey.results[m].content;
                        //console.log(Content);
                        //reviewsHTML += '<div class="col-md-4 col-sm-6">';
                        reviewsHTML += '<div class="class="block-text rel zmin">';
                        reviewsHTML += '<div class="reviewer">';
                        reviewsHTML += '<h4><b>*******' + Reviewer + '*******</b></h4>';
                        reviewsHTML += '</div>';
                        reviewsHTML += '<div class="content">';
                        reviewsHTML += '<p><i>' + Content + '</i></p>';
                        reviewsHTML += '</div>';
                        reviewsHTML += '</div>';

                    }
                })

                $.getJSON(detailUrl, function(movieKey) {
                    // console.log(i);
                    // console.log(thisMovieUrl)
                    //console.log(movieKey)


                    var poster = imageBaseUrl + 'w500' + movieKey.poster_path;
                    // console.log(poster);

                    var title = nowPlayingData.results[i].original_title;

                    var releaseDate = nowPlayingData.results[i].release_date;

                    var overview = nowPlayingData.results[i].overview;
                    // $('.overview').addClass('overview');

                    var voteAverage = nowPlayingData.results[i].vote_average;
                    // console.log(movieKey)
                    //var youtubeKey = movieKey.results[0].key;

                    var youtubeLink = 'https://www.youtube.com/';
                    // console.log(youtubeLink)
                    var nowPlayingHTML = '';

                    // nowPlayingHTML += '<div class="col-sm-3 eachMovie">';
                    nowPlayingHTML += '<div class="row"';
                    nowPlayingHTML += '<a href="' + youtubeLink + '"><img src="' + poster + '"></a>';

                    // nowPlayingHTML += '</div><br>'; //close trailerLink
                    nowPlayingHTML += '<div class="col-sm-6 movieDetails">';
                    nowPlayingHTML += '<div class="movieName">' + title + '</div><br>';
                    nowPlayingHTML += '<div class="linkToTrailer"><a href="' + youtubeLink + '"><span class="glyphicon glyphicon-play"></span>&nbspPlay trailer</a>' + '</div><br>';
                    nowPlayingHTML += '<div class="release">Release Date: ' + releaseDate + '</div><br>';

                    nowPlayingHTML += '<div class="genre"><b>Genre: </b>' + category + '</div><br>';
                    nowPlayingHTML += '<div class="overview">' + overview + '</div><br>'; // Put overview in a separate div to make it easier to style
                    nowPlayingHTML += '<div class="rating">Rating: ' + voteAverage + '/10</div><br>';

                    //nowPlayingHTML += '<div class="director-image"><img src="' + directorImage + '"></div>';
                    // nowPlayingHTML += ' <div class="navbar-header" style=" background: rgba(0, 0, 0, 0.6);" ><a class="navbar-brand" href="#" id="magicbutton">MoreInfo</a></div>';
                    // nowPlayingHTML += '<button type="submit" class="btn btn-default" onclick="' + movieDetail(mid2) + ';">MoreInfo</button>';
                    // nowPlayingHTML += '<h4 id="movieIdd" value="' + mid2 + '"></h2>';
                    nowPlayingHTML += '<h3><b>Director</b></h3>'
                    nowPlayingHTML += directorInforHTML;
                    nowPlayingHTML += '<br>';


                    nowPlayingHTML += '<div class="each-movie">';
                    nowPlayingHTML += '<h4><b>Actors</b></h4>';
                    nowPlayingHTML += '<div id="list-actor">';

                    nowPlayingHTML += actorInforHTML;
                    nowPlayingHTML += '</div>';
                    nowPlayingHTML += ' <nav aria-label=...><ul class=pagination><li id="previous-page"><a href="javascript:void(0)" aria-label=Previous><span aria-hidden=true>&laquo;</span></a></li></ul><nav>';
                    nowPlayingHTML += '</div>';
                    nowPlayingHTML += '</div>'; //close movieDetails

                    nowPlayingHTML += '</div>'; //close off each div

                    nowPlayingHTML += '<h4><b>^^^REVIEW MOVIE^^^</b></h4>';
                    nowPlayingHTML += reviewsHTML;


                    $('#movie-grid').append(nowPlayingHTML);

                    //$('#movieGenreLabel').html("Now Playing");
                    $('#movieGenreLabel').html("MOVIES DETAILS");

                    //PageChange(movieKey.cast.length);
                    makePagination(40);
                })

            }

        }
    })
}

// function movieDetail2(searchTerms) {
//     $('#movie-grid').empty();
//     alert(searchTerm);

//     var res = searchTerms.split("/");
//     var searchTerm = res[0];
//     var mid = res[1];
//     //var mid = 769;
//     $("body").removeClass("modal-open");
//     $("div").removeClass("modal-backdrop fade in");
//     var apiBaseURL = 'http://api.themoviedb.org/3/';

//     var imageBaseUrl = 'https://image.tmdb.org/t/p/';

//     var apiKey = '53ddf3f904e38ff4e8418794a5da4e88';
//     var thisMovieUrl = apiBaseURL + 'movie/' + mid + '/videos?api_key=' + apiKey;
//     // console.log(thisMovieUrl);
//     var thisMovieCharacterUrl = apiBaseURL + 'movie/' + mid + '/credits?api_key=' + apiKey;
//     // console.log(thisMovieCharacterUrl);
//     var thisMovieReviews = apiBaseURL + 'movie/' + mid + '/reviews?api_key=' + apiKey;
//     var detailUrl = apiBaseURL + 'movie/' + mid + '?api_key=' + apiKey;
//     var nowPlayingURL = apiBaseURL + 'movie/top_rated?api_key=' + apiKey;

//     const searchMovieURL = apiBaseURL + 'search/movie?api_key=' + apiKey + '&language=en-US&page=1&include_adult=false&query=' + searchTerm;

//     $.getJSON(searchMovieURL, function(nowPlayingData) {
//         var actorInforHTML = '';
//         var directorInforHTML = '';
//         for (let i = 0; i < nowPlayingData.results.length; i++) {
//             if (nowPlayingData.results[i].id === mid) {
//                 $.getJSON(thisMovieCharacterUrl, function(movieKey) {

//                     actorInforHTML = '';
//                     // console.log(movieKey);
//                     for (let k = 0; k < movieKey.cast.length; k++) {
//                         var realName = movieKey.cast[k].name;
//                         //console.log("ACTOR: " + realName);
//                         var nameCharacter = movieKey.cast[k].character;
//                         //console.log("IN DISPLAY: " + nameCharacter);
//                         var imgCharacter = imageBaseUrl + 'w300' + movieKey.cast[k].profile_path;
//                         //console.log(imgCharacter);
//                         const actorId = movieKey.cast[k].id;

//                         actorInforHTML += '<div class="block-actors">';
//                         actorInforHTML += ' <div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 535px; height: 170px; margin: 0px; overflow: hidden;">';
//                         actorInforHTML += '<ul class="row" id="list_actor_carousel" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 3531px; height: 170px; z-index: auto;"> <li style="display: inline;">';
//                         actorInforHTML += ' <a class="actor-profile-item" href="#" onclick ="actorDetail(' + actorId + ');"  title="' + realName + ' Trong Vai ' + nameCharacter + '">';
//                         actorInforHTML += '<div class="actor-image"><img src="' + imgCharacter + '"></div>';
//                         actorInforHTML += '<div class="actor-name"><span class="actor-name-a">' + realName + '</span><span class="character">' + ' ' + nameCharacter + '</span></div>';
//                         actorInforHTML += ' </a>';
//                         actorInforHTML += ' </li>';
//                         actorInforHTML += ' </ul></div></div>';
//                     }
//                     //actorInforHTML += ' <nav aria-label=...><ul class=pagination><li id="previous-page"><a href="javascript:void(0)" aria-label=Previous><span aria-hidden=true>&laquo;</span></a></li></ul><nav>';
//                     //makePagination(movieKey.cast.length);
//                     directorInforHTML = '';
//                     for (let l = 0; l < movieKey.crew.length; l++) {
//                         var jobForce = movieKey.crew[l].job;
//                         if (jobForce.localeCompare("Director") == 0) {
//                             var directorName = movieKey.crew[l].name;
//                             var directorImage = imageBaseUrl + 'w300' + movieKey.crew[l].profile_path;

//                             directorInforHTML += '<div class="block-director">';
//                             directorInforHTML += ' <a class="director-profile-item" href="#" title="' + directorName + '">';
//                             directorInforHTML += '<div class="director-image"><img src="' + directorImage + '"></div>';
//                             directorInforHTML += ' <div class="director-name" ><b>' + directorName + '</b></div>';
//                             directorInforHTML += ' </a>';
//                             directorInforHTML += '</div>';
//                             //console.log(directorImage);
//                         }
//                     }
//                 })

//                 var category;

//                 $.getJSON(detailUrl, function(movieKey) {
//                     //console.log(movieKey);
//                     category = '';
//                     for (let j = 0; j < movieKey.genres.length; j++) {
//                         if (category != '') {
//                             category = category + ', ' + movieKey.genres[j].name;
//                         } else {
//                             category = category + movieKey.genres[j].name;
//                         }
//                     }
//                     return category;
//                 })
//                 var reviewsHTML = '';
//                 $.getJSON(thisMovieReviews, function(movieKey) {
//                     reviewsHTML = '';
//                     for (let m = 0; m < 2; m++) {
//                         var Reviewer = movieKey.results[m].author;
//                         //console.log(Reviewer);
//                         var Content = movieKey.results[m].content;
//                         //console.log(Content);
//                         //reviewsHTML += '<div class="col-md-4 col-sm-6">';
//                         reviewsHTML += '<div class="class="block-text rel zmin">';
//                         reviewsHTML += '<div class="reviewer">';
//                         reviewsHTML += '<h4><b>*******' + Reviewer + '*******</b></h4>';
//                         reviewsHTML += '</div>';
//                         reviewsHTML += '<div class="content">';
//                         reviewsHTML += '<p><i>' + Content + '</i></p>';
//                         reviewsHTML += '</div>';
//                         reviewsHTML += '</div>';


//                     }
//                 })

//                 $.getJSON(detailUrl, function(movieKey) {
//                     // console.log(i);
//                     // console.log(thisMovieUrl)
//                     //console.log(movieKey)


//                     var poster = imageBaseUrl + 'w500' + movieKey.poster_path;
//                     // console.log(poster);

//                     var title = nowPlayingData.results[i].original_title;

//                     var releaseDate = nowPlayingData.results[i].release_date;

//                     var overview = nowPlayingData.results[i].overview;
//                     // $('.overview').addClass('overview');

//                     var voteAverage = nowPlayingData.results[i].vote_average;
//                     // console.log(movieKey)
//                     var youtubeKey = 0;

//                     var youtubeLink = 'https://www.youtube.com/watch?v=' + youtubeKey;
//                     // console.log(youtubeLink)
//                     var nowPlayingHTML = '';

//                     // nowPlayingHTML += '<div class="col-sm-3 eachMovie">';
//                     nowPlayingHTML += '<div class="row"';
//                     nowPlayingHTML += '<a href="' + youtubeLink + '"><img src="' + poster + '"></a>';

//                     // nowPlayingHTML += '</div><br>'; //close trailerLink
//                     nowPlayingHTML += '<div class="col-sm-6 movieDetails">';
//                     nowPlayingHTML += '<div class="movieName">' + title + '</div><br>';
//                     nowPlayingHTML += '<div class="linkToTrailer"><a href="' + youtubeLink + '"><span class="glyphicon glyphicon-play"></span>&nbspPlay trailer</a>' + '</div><br>';
//                     nowPlayingHTML += '<div class="release">Release Date: ' + releaseDate + '</div><br>';

//                     nowPlayingHTML += '<div class="genre"><b>Genre: </b>' + category + '</div><br>';
//                     nowPlayingHTML += '<div class="overview">' + overview + '</div><br>'; // Put overview in a separate div to make it easier to style
//                     nowPlayingHTML += '<div class="rating">Rating: ' + voteAverage + '/10</div><br>';

//                     //nowPlayingHTML += '<div class="director-image"><img src="' + directorImage + '"></div>';
//                     // nowPlayingHTML += ' <div class="navbar-header" style=" background: rgba(0, 0, 0, 0.6);" ><a class="navbar-brand" href="#" id="magicbutton">MoreInfo</a></div>';
//                     // nowPlayingHTML += '<button type="submit" class="btn btn-default" onclick="' + movieDetail(mid2) + ';">MoreInfo</button>';
//                     // nowPlayingHTML += '<h4 id="movieIdd" value="' + mid2 + '"></h2>';
//                     nowPlayingHTML += '<h3><b>Director</b></h3>'
//                     nowPlayingHTML += directorInforHTML;
//                     nowPlayingHTML += '<br>';


//                     nowPlayingHTML += '<div class="each-movie">';
//                     nowPlayingHTML += '<h4><b>Actors</b></h4>';
//                     nowPlayingHTML += '<div id="list-actor">';

//                     nowPlayingHTML += actorInforHTML;
//                     nowPlayingHTML += '</div>';
//                     nowPlayingHTML += ' <nav aria-label=...><ul class=pagination><li id="previous-page"><a href="javascript:void(0)" aria-label=Previous><span aria-hidden=true>&laquo;</span></a></li></ul><nav>';
//                     nowPlayingHTML += '</div>';
//                     nowPlayingHTML += '</div>'; //close movieDetails

//                     nowPlayingHTML += '</div>'; //close off each div

//                     nowPlayingHTML += '<h4><b>^^^REVIEW MOVIE^^^</b></h4>';
//                     nowPlayingHTML += reviewsHTML;


//                     $('#movie-grid').append(nowPlayingHTML);

//                     //$('#movieGenreLabel').html("Now Playing");
//                     $('#movieGenreLabel').html("MOVIES DETAILS");

//                     //PageChange(movieKey.cast.length);
//                     makePagination(40);
//                 })

//             }

//         }
//     })
// }

function actorDetail(aid) {
    $('#movie-grid').empty();
    var Aid = aid;
    //var mid = 769;
    // $("body").removeClass("modal-open");
    // $("div").removeClass("modal-backdrop fade in");
    var apiBaseURL = 'http://api.themoviedb.org/3/';

    var imageBaseUrl = 'https://image.tmdb.org/t/p/';

    var apiKey = '53ddf3f904e38ff4e8418794a5da4e88';
    var thisActorURL = apiBaseURL + 'person/' + Aid + '?api_key=' + apiKey;
    //alert(thisActorURL);
    var thisActorMovieURL = apiBaseURL + 'person/' + Aid + '/movie_credits?api_key=' + apiKey;

    $.getJSON(thisActorMovieURL, function(actorKey) {
            var listMovieDetailHTML = '';
            for (let i = 0; i < 10; i++) {
                const mvid = actorKey.cast[i].id;
                const mvname = actorKey.cast[i].title;
                var detailUrl = apiBaseURL + 'movie/' + mvid + '?api_key=' + apiKey;
                //console.log(detailUrl);
                var category;
                var productionContries;


                // + '<li><i>Genres: ' + category + '</i></li><li><i>Countries: ' + productionContries + '</i></li></li>';
                // listMovieDetailHTML += '</ul>';
                $.getJSON(detailUrl, function(movieKey) {
                        //console.log(movieKey);
                        category = '';
                        for (let j = 0; j < movieKey.genres.length; j++) {
                            if (category != '') {
                                category = category + ', ' + movieKey.genres[j].name;
                            } else {
                                category = category + movieKey.genres[j].name;
                            }
                        }
                        productionContries = '';
                        for (let k = 0; k < movieKey.production_countries.length; k++) {
                            if (productionContries != '') {
                                productionContries = productionContries + ', ' + movieKey.production_countries[k].name;
                            } else {
                                productionContries = productionContries + movieKey.production_countries[k].name;
                            }
                        }
                        listMovieDetailHTML += '<ul>';
                        listMovieDetailHTML += '<li><b>MOVIE NAME:</b>' + mvname;
                        listMovieDetailHTML += '<li><i>Genres: ' + category + '</i></li><li><i>Countries: ' + productionContries + '</i></li>';
                        listMovieDetailHTML += '</li></ul>';
                        // return productionContries;
                        //listMovieDetailHTML2 = listMovieDetailHTML;
                    })
                    //return listMovieDetailHTML;

            }
            var actorDetailHTML = '';
            $.getJSON(thisActorURL, function(actorKey) {
                actorDetailHTML = '';
                var actorNameDetail = actorKey.name;
                var actorBirthDayDetail = actorKey.birthday;
                var actorBiographyDetail = actorKey.biography;
                var actorImageDetail = imageBaseUrl + 'w500' + actorKey.profile_path;

                actorDetailHTML += '<div class="row"';
                // nowPlayingHTML += '</div><br>'; //close trailerLink
                actorDetailHTML += '<div class="col-sm-6 movieDetails">';
                actorDetailHTML += '<div class="actorName"><h1><b>' + actorNameDetail + '</b></h1></div><br>';
                actorDetailHTML += '<a href="#"><img src="' + actorImageDetail + '"></a>';
                actorDetailHTML += '<div class="birthday"><b>Birth Day: </b>' + actorBirthDayDetail + '</div><br>';
                actorDetailHTML += '<div class="biography"><b>Biography : </b>' + actorBiographyDetail + '</div><br>';
                actorDetailHTML += '</div>';
                actorDetailHTML += '</div>';
                actorDetailHTML += '<h3>LIST MOVIES PARTICIPATED</h3>'
                actorDetailHTML += listMovieDetailHTML;

                $('#movie-grid').append(actorDetailHTML);
                $('#movieGenreLabel').html("ACTOR DETAILS");
            })
        })
        //return listMovieDetailHTML;

}


function makePagination(numindexx) {
    var numberOfItems = numindexx; // Get total number of the items that should be paginated
    //console.log(numberOfItems);
    var limitPerPage = 4; // Limit of items per each page
    $("#list-actor .block-actors:gt(" + (limitPerPage - 1) + ")").hide(); // Hide all items over page limits (e.g., 5th item, 6th item, etc.)
    var totalPages = Math.round(numberOfItems / limitPerPage); // Get number of pages
    $('#movie-grid' + ' .' + each_movie).find('.' + pagination).append($('<li/>', {
        'class': 'current-page active',
        html: "<a href='javascript:void(0)'>" + 1 + "</a>"
    }));
    //.append("<li class='current-page active'><a href='javascript:void(0)'>" + 1 + "</a></li>"); // Add first page marker

    // Loop to insert page number for each sets of items equal to page limit (e.g., limit of 4 with 20 total items = insert 5 pages)
    for (var i = 2; i <= totalPages; i++) {
        // $(".pagination").append("<li class='current-page'><a href='javascript:void(0)'>" + i + "</a></li>"); // Insert page number into pagination tabs
        $('#movie-grid' + ' .' + each_movie).find('.' + pagination).append($('<li/>', {
            'class': 'current-page',
            html: "<a href='javascript:void(0)'>" + i + "</a>"
        }));
    }

    // Add next button after all the page numbers  
    // $(".pagination").append("<li id='next-page'><a href='javascript:void(0)' aria-label=Next><span aria-hidden=true>&raquo;</span></a></li>");
    $('#movie-grid' + ' .' + each_movie).find('.' + pagination).append($('<li/>', {
        'id': 'next-page',
        html: "<a href='javascript:void(0) aria-label=Next'><span aria-hidden=true>&raquo;</span></a>"
    }));

    // Function that displays new items based on page number that was clicked
    $('#movie-grid' + ' .' + each_movie).find('.' + current_page).on("click", function() {
        // Check if page number that was clicked on is the current page that is being displayed
        if ($(this).hasClass('active')) {
            return false; // Return false (i.e., nothing to do, since user clicked on the page number that is already being displayed)
        } else {
            var currentPage = $(this).index(); // Get the current page number
            $('#movie-grid' + ' .' + each_movie + ' .' + pagination).find('li').removeClass('active'); // Remove the 'active' class status from the page that is currently being displayed
            $(this).addClass('active'); // Add the 'active' class status to the page that was clicked on
            $("#list-actor .block-actors").hide(); // Hide all items in loop, this case, all the list groups
            var grandTotal = limitPerPage * currentPage; // Get the total number of items up to the page number that was clicked on

            // Loop through total items, selecting a new set of items based on page number
            for (var i = grandTotal - limitPerPage; i < grandTotal; i++) {
                $("#list-actor .block-actors:eq(" + i + ")").show(); // Show items from the new page that was selected
            }
        }

    });


    // Function to navigate to the next page when users click on the next-page id (next page button)
    $('#movie-grid' + ' .' + each_movie).find('#' + next_page).on("click", function() {
        var currentPage = $('#movie-grid' + ' .' + each_movie + ' .' + pagination).find('li.active').index(); // Identify the current active page
        // Check to make sure that navigating to the next page will not exceed the total number of pages
        if (currentPage === totalPages) {
            return false; // Return false (i.e., cannot navigate any further, since it would exceed the maximum number of pages)
        } else {
            currentPage++; // Increment the page by one
            $('#movie-grid' + ' .' + each_movie + ' .' + pagination).find('li').removeClass('active'); // Remove the 'active' class status from the current page
            $("#list-actor .block-actors").hide(); // Hide all items in the pagination loop
            var grandTotal = limitPerPage * currentPage; // Get the total number of items up to the page that was selected

            // Loop through total items, selecting a new set of items based on page number
            for (var i = grandTotal - limitPerPage; i < grandTotal; i++) {
                $("#list-actor .block-actors:eq(" + i + ")").show(); // Show items from the new page that was selected
            }

            $('#movie-grid' + ' .' + each_movie + ' .' + pagination).find('li.current-page:eq(' + (currentPage - 1) + ')').addClass('active');
            // $(".pagination li.current-page:eq(" + (currentPage - 1) + ")").addClass('active'); // Make new page number the 'active' page
        }
    });

    // Function to navigate to the previous page when users click on the previous-page id (previous page button)
    $('#movie-grid' + ' .' + each_movie).find('#' + previous_page).on("click", function() {
        var currentPage = $('#movie-grid' + ' .' + each_movie + ' .' + pagination).find('li.active').index(); // Identify the current active page
        // Check to make sure that users is not on page 1 and attempting to navigating to a previous page
        if (currentPage === 1) {
            return false; // Return false (i.e., cannot navigate to a previous page because the current page is page 1)
        } else {
            currentPage--; // Decrement page by one
            $('#movie-grid' + ' .' + each_movie + ' .' + pagination).find('li').removeClass('active'); // Remove the 'activate' status class from the previous active page number
            $("#list-actor .block-actors").hide(); // Hide all items in the pagination loop
            var grandTotal = limitPerPage * currentPage; // Get the total number of items up to the page that was selected

            // Loop through total items, selecting a new set of items based on page number
            for (var i = grandTotal - limitPerPage; i < grandTotal; i++) {
                $("#list-actor .block-actors:eq(" + i + ")").show(); // Show items from the new page that was selected
            }

            $('#movie-grid' + ' .' + each_movie + ' .' + pagination).find('li.current-page:eq(' + (currentPage - 1) + ')').addClass('active');
            // $(".pagination li.current-page:eq(" + (currentPage - 1) + ")").addClass('active'); // Make new page number the 'active' page
        }
    });
}

function movieDetail2(Mid) {
    $('#movie-grid').empty();
    var searchTerm2 = $('.form-control').val();
    //console.log(searchTerm2);
    var mid = Mid;

    //var mid = 769;
    $("body").removeClass("modal-open");
    $("div").removeClass("modal-backdrop fade in");
    var apiBaseURL = 'http://api.themoviedb.org/3/';

    var imageBaseUrl = 'https://image.tmdb.org/t/p/';

    var apiKey = '53ddf3f904e38ff4e8418794a5da4e88';
    var thisMovieUrl = apiBaseURL + 'movie/' + mid + '/videos?api_key=' + apiKey;
    // console.log(thisMovieUrl);
    let thisMovieCharacterUrl = apiBaseURL + 'movie/' + mid + '/credits?api_key=' + apiKey;
    // console.log(thisMovieCharacterUrl);
    let thisMovieReviews = apiBaseURL + 'movie/' + mid + '/reviews?api_key=' + apiKey;
    let detailUrl = apiBaseURL + 'movie/' + mid + '?api_key=' + apiKey;
    // var nowPlayingURL = apiBaseURL + 'movie/top_rated?api_key=' + apiKey;

    const searchMovieURL = apiBaseURL + 'search/movie?api_key=' + apiKey + '&language=en-US&page=1&include_adult=false&query=' + searchTerm2;

    $.getJSON(searchMovieURL, function(nowPlayingData) {
        let actorInforHTML = '';
        let directorInforHTML = '';
        for (let i = 0; i < nowPlayingData.results.length; i++) {
            if (nowPlayingData.results[i].id === mid) {
                $.getJSON(thisMovieCharacterUrl, function(movieKey) {

                    actorInforHTML = '';
                    // console.log(movieKey);
                    for (let k = 0; k < movieKey.cast.length; k++) {
                        var realName = movieKey.cast[k].name;
                        //console.log("ACTOR: " + realName);
                        var nameCharacter = movieKey.cast[k].character;
                        //console.log("IN DISPLAY: " + nameCharacter);
                        var imgCharacter = imageBaseUrl + 'w300' + movieKey.cast[k].profile_path;
                        //console.log(imgCharacter);
                        const actorId = movieKey.cast[k].id;

                        actorInforHTML += '<div class="block-actors">';
                        actorInforHTML += ' <div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 535px; height: 170px; margin: 0px; overflow: hidden;">';
                        actorInforHTML += '<ul class="row" id="list_actor_carousel" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 3531px; height: 170px; z-index: auto;"> <li style="display: inline;">';
                        actorInforHTML += ' <a class="actor-profile-item" href="#" onclick ="actorDetail(' + actorId + ');"  title="' + realName + ' Trong Vai ' + nameCharacter + '">';
                        actorInforHTML += '<div class="actor-image"><img src="' + imgCharacter + '"></div>';
                        actorInforHTML += '<div class="actor-name"><span class="actor-name-a">' + realName + '</span><span class="character">' + ' ' + nameCharacter + '</span></div>';
                        actorInforHTML += ' </a>';
                        actorInforHTML += ' </li>';
                        actorInforHTML += ' </ul></div></div>';
                    }
                    //actorInforHTML += ' <nav aria-label=...><ul class=pagination><li id="previous-page"><a href="javascript:void(0)" aria-label=Previous><span aria-hidden=true>&laquo;</span></a></li></ul><nav>';
                    //makePagination(movieKey.cast.length);
                    directorInforHTML = '';
                    for (let l = 0; l < movieKey.crew.length; l++) {
                        var jobForce = movieKey.crew[l].job;
                        if (jobForce.localeCompare("Director") == 0) {
                            var directorName = movieKey.crew[l].name;
                            var directorImage = imageBaseUrl + 'w300' + movieKey.crew[l].profile_path;

                            directorInforHTML += '<div class="block-director">';
                            directorInforHTML += ' <a class="director-profile-item" href="#" title="' + directorName + '">';
                            directorInforHTML += '<div class="director-image"><img src="' + directorImage + '"></div>';
                            directorInforHTML += ' <div class="director-name" ><b>' + directorName + '</b></div>';
                            directorInforHTML += ' </a>';
                            directorInforHTML += '</div>';
                            //console.log(directorImage);
                        }
                    }
                })

                var category;

                $.getJSON(detailUrl, function(movieKey) {
                    //console.log(movieKey);
                    category = '';
                    for (let j = 0; j < movieKey.genres.length; j++) {
                        if (category != '') {
                            category = category + ', ' + movieKey.genres[j].name;
                        } else {
                            category = category + movieKey.genres[j].name;
                        }
                    }
                    return category;
                })
                let reviewsHTML = '';
                $.getJSON(thisMovieReviews, function(movieKey) {
                    reviewsHTML = '';
                    for (let m = 0; m < 2; m++) {
                        var Reviewer = movieKey.results[m].author;
                        //console.log(Reviewer);
                        var Content = movieKey.results[m].content;
                        //console.log(Content);
                        //reviewsHTML += '<div class="col-md-4 col-sm-6">';
                        reviewsHTML += '<div class="class="block-text rel zmin">';
                        reviewsHTML += '<div class="reviewer">';
                        reviewsHTML += '<h4><b>*******' + Reviewer + '*******</b></h4>';
                        reviewsHTML += '</div>';
                        reviewsHTML += '<div class="content">';
                        reviewsHTML += '<p><i>' + Content + '</i></p>';
                        reviewsHTML += '</div>';
                        reviewsHTML += '</div>';


                    }
                })

                $.getJSON(detailUrl, function(movieKey) {
                    // console.log(i);
                    // console.log(thisMovieUrl)
                    //console.log(movieKey)


                    var poster = imageBaseUrl + 'w500' + movieKey.poster_path;
                    // console.log(poster);

                    var title = nowPlayingData.results[i].original_title;

                    var releaseDate = nowPlayingData.results[i].release_date;

                    var overview = nowPlayingData.results[i].overview;
                    // $('.overview').addClass('overview');

                    var voteAverage = nowPlayingData.results[i].vote_average;
                    // console.log(movieKey)
                    var youtubeKey = 0;

                    var youtubeLink = 'https://www.youtube.com/watch?v=' + youtubeKey;
                    // console.log(youtubeLink)
                    var nowPlayingHTML = '';

                    // nowPlayingHTML += '<div class="col-sm-3 eachMovie">';
                    nowPlayingHTML += '<div class="row"';
                    nowPlayingHTML += '<a href="' + youtubeLink + '"><img src="' + poster + '"></a>';

                    // nowPlayingHTML += '</div><br>'; //close trailerLink
                    nowPlayingHTML += '<div class="col-sm-6 movieDetails">';
                    nowPlayingHTML += '<div class="movieName">' + title + '</div><br>';
                    nowPlayingHTML += '<div class="linkToTrailer"><a href="' + youtubeLink + '"><span class="glyphicon glyphicon-play"></span>&nbspPlay trailer</a>' + '</div><br>';
                    nowPlayingHTML += '<div class="release">Release Date: ' + releaseDate + '</div><br>';

                    nowPlayingHTML += '<div class="genre"><b>Genre: </b>' + category + '</div><br>';
                    nowPlayingHTML += '<div class="overview">' + overview + '</div><br>'; // Put overview in a separate div to make it easier to style
                    nowPlayingHTML += '<div class="rating">Rating: ' + voteAverage + '/10</div><br>';

                    //nowPlayingHTML += '<div class="director-image"><img src="' + directorImage + '"></div>';
                    // nowPlayingHTML += ' <div class="navbar-header" style=" background: rgba(0, 0, 0, 0.6);" ><a class="navbar-brand" href="#" id="magicbutton">MoreInfo</a></div>';
                    // nowPlayingHTML += '<button type="submit" class="btn btn-default" onclick="' + movieDetail(mid2) + ';">MoreInfo</button>';
                    // nowPlayingHTML += '<h4 id="movieIdd" value="' + mid2 + '"></h2>';
                    nowPlayingHTML += '<h3><b>Director</b></h3>'
                    nowPlayingHTML += directorInforHTML;
                    nowPlayingHTML += '<br>';


                    nowPlayingHTML += '<div class="each-movie">';
                    nowPlayingHTML += '<h4><b>Actors</b></h4>';
                    nowPlayingHTML += '<div id="list-actor">';

                    nowPlayingHTML += actorInforHTML;
                    nowPlayingHTML += '</div>';
                    nowPlayingHTML += ' <nav aria-label=...><ul class=pagination><li id="previous-page"><a href="javascript:void(0)" aria-label=Previous><span aria-hidden=true>&laquo;</span></a></li></ul><nav>';
                    nowPlayingHTML += '</div>';
                    nowPlayingHTML += '</div>'; //close movieDetails

                    nowPlayingHTML += '</div>'; //close off each div

                    nowPlayingHTML += '<h4><b>^^^REVIEW MOVIE^^^</b></h4>';
                    nowPlayingHTML += reviewsHTML;


                    $('#movie-grid').append(nowPlayingHTML);

                    //$('#movieGenreLabel').html("Now Playing");
                    $('#movieGenreLabel').html("MOVIES DETAILS");

                    //PageChange(movieKey.cast.length);
                    makePagination(40);
                })

            }

        }
    })
}

function movieDetail3(Mid) {
    $('#movie-grid').empty();
    var searchTerm3 = $('.form-control2').val();
    //console.log(searchTerm2);

    var mid = Mid;

    //var mid = 769;
    $("body").removeClass("modal-open");
    $("div").removeClass("modal-backdrop fade in");
    var apiBaseURL = 'http://api.themoviedb.org/3/';

    var imageBaseUrl = 'https://image.tmdb.org/t/p/';

    var apiKey = '53ddf3f904e38ff4e8418794a5da4e88';
    var thisMovieUrl = apiBaseURL + 'movie/' + mid + '/videos?api_key=' + apiKey;
    // console.log(thisMovieUrl);
    let thisMovieCharacterUrl = apiBaseURL + 'movie/' + mid + '/credits?api_key=' + apiKey;
    // console.log(thisMovieCharacterUrl);
    let thisMovieReviews = apiBaseURL + 'movie/' + mid + '/reviews?api_key=' + apiKey;
    let detailUrl = apiBaseURL + 'movie/' + mid + '?api_key=' + apiKey;
    // var nowPlayingURL = apiBaseURL + 'movie/top_rated?api_key=' + apiKey;

    //const searchMovieURL = apiBaseURL + 'search/person?api_key=' + apiKey + '&language=en-US&page=1&include_adult=false&query=' + searchTerm3;


    //$.getJSON(searchMovieURL, function(nowPlayingData) {
    let actorInforHTML = '';
    let directorInforHTML = '';
    $.getJSON(thisMovieCharacterUrl, function(movieKey) {

        actorInforHTML = '';
        // console.log(movieKey);
        for (let k = 0; k < movieKey.cast.length; k++) {
            var realName = movieKey.cast[k].name;
            //console.log("ACTOR: " + realName);
            var nameCharacter = movieKey.cast[k].character;
            //console.log("IN DISPLAY: " + nameCharacter);
            var imgCharacter = imageBaseUrl + 'w300' + movieKey.cast[k].profile_path;
            //console.log(imgCharacter);
            const actorId = movieKey.cast[k].id;

            actorInforHTML += '<div class="block-actors">';
            actorInforHTML += ' <div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 535px; height: 170px; margin: 0px; overflow: hidden;">';
            actorInforHTML += '<ul class="row" id="list_actor_carousel" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 3531px; height: 170px; z-index: auto;"> <li style="display: inline;">';
            actorInforHTML += ' <a class="actor-profile-item" href="#" onclick ="actorDetail(' + actorId + ');"  title="' + realName + ' Trong Vai ' + nameCharacter + '">';
            actorInforHTML += '<div class="actor-image"><img src="' + imgCharacter + '"></div>';
            actorInforHTML += '<div class="actor-name"><span class="actor-name-a">' + realName + '</span><span class="character">' + ' ' + nameCharacter + '</span></div>';
            actorInforHTML += ' </a>';
            actorInforHTML += ' </li>';
            actorInforHTML += ' </ul></div></div>';
        }
        //actorInforHTML += ' <nav aria-label=...><ul class=pagination><li id="previous-page"><a href="javascript:void(0)" aria-label=Previous><span aria-hidden=true>&laquo;</span></a></li></ul><nav>';
        //makePagination(movieKey.cast.length);
        directorInforHTML = '';
        for (let l = 0; l < movieKey.crew.length; l++) {
            var jobForce = movieKey.crew[l].job;
            if (jobForce.localeCompare("Director") == 0) {
                var directorName = movieKey.crew[l].name;
                var directorImage = imageBaseUrl + 'w300' + movieKey.crew[l].profile_path;

                directorInforHTML += '<div class="block-director">';
                directorInforHTML += ' <a class="director-profile-item" href="#" title="' + directorName + '">';
                directorInforHTML += '<div class="director-image"><img src="' + directorImage + '"></div>';
                directorInforHTML += ' <div class="director-name" ><b>' + directorName + '</b></div>';
                directorInforHTML += ' </a>';
                directorInforHTML += '</div>';
                //console.log(directorImage);
            }
        }
    })

    var category;

    $.getJSON(detailUrl, function(movieKey) {
        //console.log(movieKey);
        category = '';
        for (let j = 0; j < movieKey.genres.length; j++) {
            if (category != '') {
                category = category + ', ' + movieKey.genres[j].name;
            } else {
                category = category + movieKey.genres[j].name;
            }
        }
        return category;
    })
    let reviewsHTML = '';
    $.getJSON(thisMovieReviews, function(movieKey) {
        reviewsHTML = '';
        for (let m = 0; m < 2; m++) {
            var Reviewer = movieKey.results[m].author;
            //console.log(Reviewer);
            var Content = movieKey.results[m].content;
            //console.log(Content);
            //reviewsHTML += '<div class="col-md-4 col-sm-6">';
            reviewsHTML += '<div class="class="block-text rel zmin">';
            reviewsHTML += '<div class="reviewer">';
            reviewsHTML += '<h4><b>*******' + Reviewer + '*******</b></h4>';
            reviewsHTML += '</div>';
            reviewsHTML += '<div class="content">';
            reviewsHTML += '<p><i>' + Content + '</i></p>';
            reviewsHTML += '</div>';
            reviewsHTML += '</div>';


        }
    })

    $.getJSON(detailUrl, function(movieKey) {
            // console.log(i);
            // console.log(thisMovieUrl)
            //console.log(movieKey)


            var poster = imageBaseUrl + 'w500' + movieKey.poster_path;
            // console.log(poster);

            var title = movieKey.title;
            //console.log(title);

            var releaseDate = movieKey.release_date;

            var overview = movieKey.overview;
            // $('.overview').addClass('overview');

            var voteAverage = movieKey.vote_average;
            // console.log(movieKey)


            var youtubeLink = 'https://www.youtube.com/watch?v=0';
            // console.log(youtubeLink)
            var nowPlayingHTML = '';

            // nowPlayingHTML += '<div class="col-sm-3 eachMovie">';
            nowPlayingHTML += '<div class="row"';
            nowPlayingHTML += '<a href="' + youtubeLink + '"><img src="' + poster + '"></a>';

            // nowPlayingHTML += '</div><br>'; //close trailerLink
            nowPlayingHTML += '<div class="col-sm-6 movieDetails">';
            nowPlayingHTML += '<div class="movieName">' + title + '</div><br>';
            nowPlayingHTML += '<div class="linkToTrailer"><a href="' + youtubeLink + '"><span class="glyphicon glyphicon-play"></span>&nbspPlay trailer</a>' + '</div><br>';
            nowPlayingHTML += '<div class="release">Release Date: ' + releaseDate + '</div><br>';

            nowPlayingHTML += '<div class="genre"><b>Genre: </b>' + category + '</div><br>';
            nowPlayingHTML += '<div class="overview">' + overview + '</div><br>'; // Put overview in a separate div to make it easier to style
            nowPlayingHTML += '<div class="rating">Rating: ' + voteAverage + '/10</div><br>';

            //nowPlayingHTML += '<div class="director-image"><img src="' + directorImage + '"></div>';
            // nowPlayingHTML += ' <div class="navbar-header" style=" background: rgba(0, 0, 0, 0.6);" ><a class="navbar-brand" href="#" id="magicbutton">MoreInfo</a></div>';
            // nowPlayingHTML += '<button type="submit" class="btn btn-default" onclick="' + movieDetail(mid2) + ';">MoreInfo</button>';
            // nowPlayingHTML += '<h4 id="movieIdd" value="' + mid2 + '"></h2>';
            nowPlayingHTML += '<h3><b>Director</b></h3>'
            nowPlayingHTML += directorInforHTML;
            nowPlayingHTML += '<br>';


            nowPlayingHTML += '<div class="each-movie">';
            nowPlayingHTML += '<h4><b>Actors</b></h4>';
            nowPlayingHTML += '<div id="list-actor">';

            nowPlayingHTML += actorInforHTML;
            nowPlayingHTML += '</div>';
            nowPlayingHTML += ' <nav aria-label=...><ul class=pagination><li id="previous-page"><a href="javascript:void(0)" aria-label=Previous><span aria-hidden=true>&laquo;</span></a></li></ul><nav>';
            nowPlayingHTML += '</div>';
            nowPlayingHTML += '</div>'; //close movieDetails

            nowPlayingHTML += '</div>'; //close off each div

            nowPlayingHTML += '<h4><b>^^^REVIEW MOVIE^^^</b></h4>';
            nowPlayingHTML += reviewsHTML;


            $('#movie-grid').append(nowPlayingHTML);

            //$('#movieGenreLabel').html("Now Playing");
            $('#movieGenreLabel').html("MOVIES DETAILS");

            //PageChange(movieKey.cast.length);
            makePagination(40);
        })
        // for (let i = 0; i < nowPlayingData.results.length; i++) {
        //     if (nowPlayingData.results[i].id === mid) {


    //     }

    // }
    //})
}

//makePagination(1);